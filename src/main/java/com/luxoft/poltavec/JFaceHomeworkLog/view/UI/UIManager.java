package main.java.com.luxoft.poltavec.JFaceHomeworkLog.view.UI;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.widgets.MessageBox;

import main.java.com.luxoft.poltavec.JFaceHomeworkLog.model.Intern;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.Events;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.InternListService;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.ProvidableView;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.Notifiable;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.Notifier;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.UpdatebleView;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.view.ButtonComposite;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.view.DataTable;

/**
 * TableButtonUI class the main class that serves the user interface Contains a
 * tableView and buttonView Contains business logic of methods used by listeners
 */
public class UIManager implements Notifiable {

	private UpdatebleView tableView;
	private ProvidableView buttonView;
	private MessageBox dialog;

	public UIManager(SashForm compositeTable) {
		this.tableView = new DataTable(compositeTable);
		this.buttonView = new ButtonComposite(compositeTable);
		this.dialog = new MessageBox(buttonView.getShell(), SWT.ICON_QUESTION | SWT.YES | SWT.NO);

		buttonView.addListeners();
		tableView.addListeners();

		Notifier.getUIManagerService().addSubscriber(this);
	}

	/**
	 * The preparation() method returns the new data model based on the one entered
	 * in the editor fields
	 */
	private Intern preparation() {
		boolean swtDone = buttonView.getSWTdoneButton();
		String name = buttonView.getTextName();
		String group = buttonView.getTextGroup();
		return new Intern(name, group, swtDone);
	}

	/**
	 * The findIndex(Intern intern) method looks for data model index from the list
	 * of models
	 */
	private int findIndex(Intern intern) {
		int index = -1;

		for (int i = 0; i < InternListService.getInternListService().getListInterns().getInterns().size(); i++) {
			if (intern.equals(InternListService.getInternListService().getListInterns().getInterns().get(i)))
				index = i;
		}
		return index;
	}

	@Override
	public void update(Events event) {
		switch (event) {
		case MODIFY:
			if (isNameFieldFill() || isGroupFieldFill()) {
				buttonView.setEnabledCancelButton(true);
				Notifier.getUIManagerService().notifySubscribers(Events.CANCELON);
			} else {
				buttonView.setEnabledCancelButton(false);
				Notifier.getUIManagerService().notifySubscribers(Events.CANCELOFF);
			}

			if (isNameFieldFill() && isGroupFieldFill()) {
				buttonView.setEnabledNewButton(true);
				Notifier.getUIManagerService().notifySubscribers(Events.NEWON);

				if (isEnteredExist() && isThereSelection()) {
					buttonView.setEnabledDeleteButton(true);
					buttonView.setEnabledSaveButton(false);
					Notifier.getUIManagerService().notifySubscribers(Events.DELETEONSAVEOFF);
				} else {
					if (isThereSelection())
						buttonView.setEnabledSaveButton(true);
					buttonView.setEnabledDeleteButton(false);
					Notifier.getUIManagerService().notifySubscribers(Events.SAVEONDELETEOFF);
				}

			} else {
				buttonView.setEnabledNewButton(false);
				buttonView.setEnabledSaveButton(false);
				buttonView.setEnabledDeleteButton(false);
				Notifier.getUIManagerService().notifySubscribers(Events.NEWSAVEDALETEOFF);
			}
			break;
		case SELECT:
			fillBySelection();
			break;
		case ADD:
			if (!isEnteredExist()) {
				dialog.setText("Add new");
				dialog.setMessage("Want add new intern?");

				int result = dialog.open();
				switch (result) {
				case SWT.YES:
					add();
					break;
				case SWT.NO:
					break;
				}
			} else {

				dialog.setText("Intern already exist");
				dialog.setMessage("Such an intern already exists, add anyway?");

				int result = dialog.open();
				switch (result) {
				case SWT.YES:
					add();
					break;
				case SWT.NO:
					break;
				}
			}
			break;
		case SAVE:

			dialog.setText("Save");
			dialog.setMessage("Want to save changes?");

			int result = dialog.open();
			switch (result) {
			case SWT.YES:
				change();
				break;
			case SWT.NO:
				break;
			}
			break;
		case DELETE:
			dialog.setText("Delete");
			dialog.setMessage("Want to delete selected entry?");

			result = dialog.open();
			switch (result) {
			case SWT.YES:
				delete();
				break;
			case SWT.NO:
				break;
			}
			break;
		case CANCEL:
			if ((!isEnteredExist()) && (isThereSelection())) {

				dialog.setText("Cancel");
				dialog.setMessage("Want to discard your changes?");

				result = dialog.open();
				switch (result) {
				case SWT.YES:

					fillBySelection();
					break;

				case SWT.NO:
					break;
				}
			} else {
				dialog.setText("Clear");
				dialog.setMessage("Want to clear filds?");

				result = dialog.open();
				switch (result) {
				case SWT.YES:

					clearEntered();
					break;

				case SWT.NO:
					break;

				}
			}
			break;
		default:
			break;
		}
	}

	/**
	 * The clearEntered() method clears the input fields and deselects the model in
	 * the table
	 */
	private void clearEntered() {
		buttonView.updateTexts(new Intern());
		tableView.setSelect(new Intern());
	}

	/**
	 * The add() method adds a data model to the list, updates the displayed list,
	 * and clears the input field in the editor
	 */
	private void add() {
		Intern intern = preparation();
		InternListService.getInternListService().getListInterns().addLearnebleTolist(intern);
		tableView.update();
		tableView.setSelect(intern);
		buttonView.updateTexts(intern);
	}

	/**
	 * The change() method changes a data model in the list, updates the displayed
	 * list, and clears the input field in the editor
	 */
	private void change() {
		int index = findIndex(tableView.getSelect());
		Intern intern = preparation();
		InternListService.getInternListService().getListInterns().changeLearnebleInList(index, intern);
		tableView.update();
		tableView.setSelect(intern);
		buttonView.updateTexts(intern);
	}

	/**
	 * The delete() method deletes a data model from the list, updates the displayed
	 * list, and clears the input field in the editor
	 */
	private void delete() {
		int index = findIndex(tableView.getSelect());
		InternListService.getInternListService().getListInterns().deleteLearnebleFromList(index);
		tableView.update();
		clearEntered();
	}

	/**
	 * The isEnteredExist() method returns True if the data model exists in the list
	 * of models
	 */
	private boolean isEnteredExist() {
		Intern intern = preparation();
		return !(findIndex(intern) == -1);
	}

	/**
	 * The isEnteredExist() method returns True if the editor fields is filled
	 */
	private boolean isThereSelection() {
		try {
			return tableView.getSelect().getClass() != null;
		} catch (NullPointerException e) {
			return false;
		}
	}

	/**
	 * The fillBySelection() method fills the editor fields by selected data model
	 * in table UI
	 */
	private void fillBySelection() {
		try {
			buttonView.updateTexts(tableView.getSelect());
		} catch (NullPointerException e) {
		}
	}

	/**
	 * The isNameFieldFill() method returns True if Name field is filled
	 */
	private boolean isNameFieldFill() {
		return (buttonView.getTextName() != "" && buttonView.getTextName() != null);
	}

	/**
	 * The isGroupFieldFill() method returns True if Group field is filled
	 */
	private boolean isGroupFieldFill() {
		return (buttonView.getTextGroup() != "" && buttonView.getTextName() != null);
	}
}
