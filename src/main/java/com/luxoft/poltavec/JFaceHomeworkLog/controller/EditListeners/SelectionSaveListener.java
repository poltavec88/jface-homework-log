package main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.EditListeners;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.Events;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.Notifier;

/**
 * The SelectionSaveListener class implements SelectionListener.
 * Here is the implementation of the Save button from the editor UI
 */	
public class SelectionSaveListener implements SelectionListener {

	@Override
	public void widgetSelected(SelectionEvent p0) {
		Notifier.getUIManagerService().notifySubscribers(Events.SAVE);
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent p0) {
	}
}