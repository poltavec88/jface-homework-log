package main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.providers;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;

import main.java.com.luxoft.poltavec.JFaceHomeworkLog.model.Intern;

/**
 * The InternViewerSorter class extends ViewerComparator and implements the model of sorting table items(data model objects)
 */
public class InternViewerSorter extends ViewerComparator   {
	
	private static final int ASCENDING = 0;
	  private static final int DESCENDING = 1;
	  private int column = 0;
	  private int direction = 0;
	  
	  public void doSort(int column) {
		    if (column == this.column) {
		      direction = 1 - direction;
		    } else {
		      this.column = column;
		      direction = ASCENDING;
		    }
	  }

	  @Override
	  public int compare(Viewer viewer, Object e1, Object e2) {
		    int result = 0;
		    Intern intern1 = (Intern) e1;
		    Intern intern2 = (Intern) e2;
		    
		    boolean x = intern1.isSWTdone();
		    boolean y = intern2.isSWTdone();
		    switch (column) {
		    case 0:
		      result = intern1.getName().compareTo(intern2.getName());
		      break;
		    case 1:
		      result = intern1.getGroup().compareTo(intern2.getGroup());
		      break;
		    case 2:
		      result = x == y ? 0 : (x == true && y == false) ? 1 : -1;
		      break;
		    }

		    if (direction == DESCENDING)
		      result = -result;

		    return result;
	  }
}
