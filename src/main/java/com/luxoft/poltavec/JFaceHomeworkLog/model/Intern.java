package main.java.com.luxoft.poltavec.JFaceHomeworkLog.model;

/**
 * The Intern class implements the model of the input data into the table
 */
public class Intern {
	
	private String name;
	private String group;
	private boolean SWTdone;
	
	public Intern(String name, String group, boolean sWTdone) {
		this.name = name;
		this.group = group;
		SWTdone = sWTdone;
	}
	
	public Intern() {
		this("", "", false);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public boolean isSWTdone() {
		return SWTdone;
	}

	public void setSWTdone(boolean sWTdone) {
		SWTdone = sWTdone;
	}

	public boolean equals(Object obj) {
		if (this == obj) return true;
	    if (obj == null) return false;
	    if (getClass() != obj.getClass()) return false;
	    
	    Intern other = (Intern) obj;
	    
	    if (!name.equals(other.name)) return false;
	    if (!group.equals(other.group)) return false;
	    if (SWTdone != other.SWTdone) return false;
	    
	    return true;
	}

	@Override
	public String toString() {
		return this.name + " - " + this.group + " = " + this.SWTdone;
	}
	
	
}
