package main.java.com.luxoft.poltavec.JFaceHomeworkLog.view;

import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import main.java.com.luxoft.poltavec.JFaceHomeworkLog.DAO.JsonWriter;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.view.UI.UIManager;

public class Window extends ApplicationWindow {

	private SashForm compositeTable;
	private final int SWT_OK_RESPONSE_CODE = 64;

	private final int APPLICATION_WINDOW_WIDTH = 900;
	private final int APPLICATION_WINDOW_HEIGHT = 450;
	private final String APPLICATION_WINDOW_NAME = "JFace homework log";
	
	private MyMenu menu;

	public Window() {
		super(null);
		addStatusLine();
	}
	
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText(APPLICATION_WINDOW_NAME);
		shell.setSize(APPLICATION_WINDOW_WIDTH, APPLICATION_WINDOW_HEIGHT);
		setStatus("Ready");
	}
	
	@Override
	protected Control createContents(Composite parent) {

		menu = new MyMenu(this);

		compositeTable = new SashForm(parent, SWT.NONE);
		compositeTable.setLayout(new FillLayout(SWT.HORIZONTAL));

		new UIManager(compositeTable);
				
		menu.addListeners();

		return parent;
	}

	@Override
	protected StatusLineManager createStatusLineManager() {
		return super.createStatusLineManager();
	}

	@Override
	protected void handleShellCloseEvent() {
		setReturnCode(CANCEL);

		MessageBox dialog = new MessageBox(this.getShell(), SWT.ICON_QUESTION | SWT.YES | SWT.NO);
		dialog.setText("Window closing");
		dialog.setMessage("Wanna save changes?");

		int result = dialog.open();
		if (result == SWT_OK_RESPONSE_CODE)
			JsonWriter.writeJson();

		close();
	}
}