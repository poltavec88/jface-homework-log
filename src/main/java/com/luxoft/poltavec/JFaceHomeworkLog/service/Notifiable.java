package main.java.com.luxoft.poltavec.JFaceHomeworkLog.service;

public interface Notifiable {
		
	public void update(Events event);

}
