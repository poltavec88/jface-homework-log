package main.java.com.luxoft.poltavec.JFaceHomeworkLog.service;

public enum Events {

	MODIFY, 
	SELECT, 
	ADD, 
	SAVE, 
	DELETE, 
	CANCEL, 
	CANCELON, 
	CANCELOFF, 
	NEWON, 
	DELETEONSAVEOFF, 
	SAVEONDELETEOFF, 
	NEWSAVEDALETEOFF
	
}
