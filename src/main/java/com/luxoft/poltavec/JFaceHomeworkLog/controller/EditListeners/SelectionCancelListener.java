package main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.EditListeners;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.Events;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.Notifier;

/**
 * The SelectionCancelListener class implements SelectionListener.
 * Here is the implementation of the Cancel button from the editor UI
 */	
public class SelectionCancelListener implements SelectionListener {

	@Override
	public void widgetSelected(SelectionEvent e) {
		Notifier.getUIManagerService().notifySubscribers(Events.CANCEL);
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
	}
}
