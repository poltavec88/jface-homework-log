package main.java.com.luxoft.poltavec.JFaceHomeworkLog.DAO;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.model.InternList;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.InternListService;

/**
 * The JsonReader class writes the contents of the file to the table.
 */	
public class JsonReader {
	
	private static Logger log = Logger.getLogger(JsonReader.class.getName());
	private static File file = JsonWriter.getFile();

	/**
	 * The readJson() method writes the contents of the file to the table.
	 */	
	public static void readJson() {
		
		try {
			JsonWriter.checkDirectory();
			ObjectMapper objectMapper = new ObjectMapper();
			InternList internList =(InternList) objectMapper.readValue(file, InternList.class);
			
			InternListService.setModifiable(internList);
			
		}catch (Exception e) {
			log.log(Level.WARNING, "File read error");
		}
		
	}
}
