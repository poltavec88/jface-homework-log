package main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.providers;

import org.eclipse.jface.viewers.IStructuredContentProvider;

import main.java.com.luxoft.poltavec.JFaceHomeworkLog.model.InternList;

public class InternContentProvider implements IStructuredContentProvider {

	@Override
	public Object[] getElements(Object interns) {
		
		return ((InternList) interns).getInterns().toArray();
	}

}
