package main.java.com.luxoft.poltavec.JFaceHomeworkLog.model;

import java.util.ArrayList;
import java.util.List;

import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.ModifiableList;

/**
 * The Intern class implements a list model of input data models into a table
 */
public class InternList implements ModifiableList {
	
	private final List<Intern> interns = new ArrayList<>();

    /**
     * The addLearnebleTolist(Intern intern) method adds a data model object to the list
     */
	@Override
	public void addLearnebleTolist(Intern intern) {
		interns.add(intern);
	}

    /**
     * The deleteLearnebleFromList(int index) method deletes a data model object from the list
     */
	@Override
	public void deleteLearnebleFromList(int index) {
		interns.remove(index);
	}

    /**
     * The getInterns() method returns the list of data model objects
     */
	@Override
	public List<Intern> getInterns() {
		return interns;
	}

    /**
     * The changeLearnebleInList(int oldLearnebleIndex, Intern newIntern) method changes a data model object from the list 
     * to a new one data model object
     */
	@Override
	public void changeLearnebleInList(int oldLearnebleIndex, Intern newIntern) {
		if ( oldLearnebleIndex < interns.size()) {
			interns.set(oldLearnebleIndex, newIntern);
		}
		else System.out.print("Illegal index");
	}
}
