package main.java.com.luxoft.poltavec.JFaceHomeworkLog.service;

import java.util.List;

import main.java.com.luxoft.poltavec.JFaceHomeworkLog.model.Intern;

public interface ModifiableList {
		
	public void addLearnebleTolist(Intern Intern);
	
	public void deleteLearnebleFromList(int index);

	public void changeLearnebleInList(int oldLearnebleIndex, Intern newIntern);
	
	public List<Intern> getInterns();
}
