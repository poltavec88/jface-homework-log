package main.java.com.luxoft.poltavec.JFaceHomeworkLog.service;

import main.java.com.luxoft.poltavec.JFaceHomeworkLog.model.InternList;

public class InternListService {

	private static ModifiableList listInterns = new InternList();
	private static InternListService internListService;

	private InternListService() {
	}

	public static InternListService getInternListService() {
		if (internListService == null) {
			return internListService = new InternListService();
		}
		return internListService;
	}
	
	/**
	 * The getModifiable() method returns the list of data models
	 */
	public ModifiableList getListInterns() {
		return listInterns;
	}

	/**
	 * The setModifiable(Modifiable modifiable) method sets the list of data models
	 */
	public static void setModifiable(ModifiableList modifiable) {
		listInterns = modifiable;
	}
}