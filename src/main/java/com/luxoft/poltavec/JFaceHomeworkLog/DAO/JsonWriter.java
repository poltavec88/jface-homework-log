package main.java.com.luxoft.poltavec.JFaceHomeworkLog.DAO;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.fasterxml.jackson.databind.ObjectMapper;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.model.InternList;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.InternListService;

/**
 * The JsonWriter class writes the contents of the table to a file
 */
public class JsonWriter {
	
	private static final File FILE = new File("history/list.json");
	private final static Path PATH = Paths.get("history");
	private static Logger log = Logger.getLogger(JsonReader.class.getName());

	/**
	 * The writeJson() method writes the contents of the table to a file and check the directory existence.
	 */	
	public static void writeJson() {
		
		try {			
			checkDirectory();
			
			ObjectMapper objectMapper = new ObjectMapper();
			InternList internList = (InternList) InternListService.getInternListService().getListInterns();
		
			objectMapper.writeValue(FILE, internList);
			
		}catch (Exception e) {
			log.log(Level.WARNING, "File write error");
		}
		
	}

	public static File getFile() {
		return FILE;
	}
	
	/**
	 * The checkDirectory() method check the directory existence.
	 */	
	protected static void checkDirectory() {
		if (!(Files.exists(PATH))) {
			try {
				Files.createDirectories(PATH);
			} catch (IOException e) {
				log.log(Level.WARNING, "File directory check error");
			}
		}
	}

}
