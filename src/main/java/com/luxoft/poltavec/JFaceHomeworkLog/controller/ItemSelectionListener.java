package main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;

import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.Events;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.Notifier;

/**
 * The ItemSelectionListener class implements ISelectionChangedListener.
 * The class notifies the user interface to display the selected item in the table in the editor user interface
 */	
public class ItemSelectionListener implements ISelectionChangedListener {
	
	@Override
	public void selectionChanged(SelectionChangedEvent arg0) {
		Notifier.getUIManagerService().notifySubscribers(Events.SELECT);
	}

}