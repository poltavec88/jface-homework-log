package main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.vertification;

import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

/**
 * The GroupVerfiticationListener class implements Listener.
 * Here is the implementation of verifying the input Group name in the editor UI
 */
public class GroupVerfiticationListener implements Listener {
	
	private final int LAST_CYRILLIC_LETTER_CHAR_NUMBER = 1103;
	private final int AMPERSAND_CHAR_NUMBER = 38;

	@Override
	public void handleEvent(Event e) {
		
		String string = e.text;
        char[] chars = new char[string.length()];
        string.getChars(0, chars.length, chars, 0);
        
        for (int i = 0; i < chars.length; i++) {	        	        	
        	if (!(AMPERSAND_CHAR_NUMBER <= chars[i] && chars[i] <= LAST_CYRILLIC_LETTER_CHAR_NUMBER)) {
        		e.doit = false;
        		return;
        	}
        }
	}
}
