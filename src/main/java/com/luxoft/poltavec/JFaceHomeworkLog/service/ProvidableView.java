package main.java.com.luxoft.poltavec.JFaceHomeworkLog.service;

import org.eclipse.swt.widgets.Shell;

import main.java.com.luxoft.poltavec.JFaceHomeworkLog.model.Intern;

public interface ProvidableView {

	public String getTextName();

	public String getTextGroup();

	public boolean getSWTdoneButton();
	
	public void setEnabledNewButton(boolean bool);
	
	public void setEnabledSaveButton(boolean bool);
	
	public void setEnabledDeleteButton(boolean bool);
	
	public void setEnabledCancelButton(boolean bool);
	
	public void updateTexts(Intern intern);
	
	public void addListeners();
	
	public Shell getShell();
	
}
