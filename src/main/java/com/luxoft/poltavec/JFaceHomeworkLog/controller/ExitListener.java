package main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller;

import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.MessageBox;

import main.java.com.luxoft.poltavec.JFaceHomeworkLog.DAO.JsonWriter;

/**
 * The ExitListener class implements SelectionListener.
 * Here is the implementation of the Exit button from the top menu
 */	
public class ExitListener implements SelectionListener {

	private ApplicationWindow parent;
	
	private final int SWT_OK_RESPONSE_CODE = 32;

	public ExitListener(ApplicationWindow applicationWindow) {
		this.parent = applicationWindow;
	}

	@Override
	public void widgetSelected(SelectionEvent e) {

		MessageBox dialog = new MessageBox(parent.getShell(), SWT.ICON_QUESTION | SWT.YES | SWT.NO);
		dialog.setText("Window closing");
		dialog.setMessage("Wanna save changes?");

		int result = dialog.open();
		if (result == SWT_OK_RESPONSE_CODE)
			JsonWriter.writeJson();

		this.parent.close();

	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		// TODO Auto-generated method stub

	}

}
