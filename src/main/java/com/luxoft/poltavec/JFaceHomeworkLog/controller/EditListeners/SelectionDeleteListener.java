package main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.EditListeners;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.Events;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.Notifier;

/**
 * The SelectionDeleteListener class implements SelectionListener.
 * Here is the implementation of the Delete button from the editor UI
 */	
public class SelectionDeleteListener implements SelectionListener {

	@Override
	public void widgetSelected(SelectionEvent p0) {
		Notifier.getUIManagerService().notifySubscribers(Events.DELETE);
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent p0) {
	}
}