package main.java.com.luxoft.poltavec.JFaceHomeworkLog.service;

import main.java.com.luxoft.poltavec.JFaceHomeworkLog.model.Intern;

public interface UpdatebleView {
	
	public void update();
	
	public Intern getSelect();
	
	public void setSelect(Intern intern);
	
	public void addListeners();
}
