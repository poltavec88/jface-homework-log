package main.java.com.luxoft.poltavec.JFaceHomeworkLog.view;

import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import main.java.com.luxoft.poltavec.JFaceHomeworkLog.DAO.JsonReader;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.ItemSelectionListener;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.providers.InternContentProvider;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.providers.InternLabelProvider;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.providers.InternViewerSorter;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.model.Intern;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.InternListService;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.UpdatebleView;

public class DataTable extends TableViewer implements UpdatebleView{
	
	public static final String FIRST_COLUMN_HEAD_NAME = "Name";
	private  final int FIRST_COLUMN_NUMBER = 0;
	public static final String SECOND_COLUMN_HEAD_NAME = "Group";
	private  final int SECOND_COLUMN_NUMBER = 1;
	public static final String THIRD_COLUMN_HEAD_NAME = "SWT done";
	private  final int THIRD_COLUMN_NUMBER = 2;
	private  final Color TABLE_HEAD_BACKGROUND_COLOR = new Color(200, 200, 200);
		
	private final int NAME_COLUMN_WIDTH = 270;
	
	
	public DataTable(Composite owner) {
		super(owner, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.FULL_SELECTION);
		createContents();
	}
	
	@Override
	public Intern getSelect() {
		  return (Intern) ((StructuredSelection) getSelection()).getFirstElement();
	}
	
	@Override
	public void setSelect(Intern item) {
		this.setSelection(new StructuredSelection(item),true);
	}

	@Override
	public void update() {
		refresh();
	}
	
	@Override
	public void addListeners() {
		this.addSelectionChangedListener(new ItemSelectionListener());
	}
	
	private void createContents() {	

	    // Set up the table
	    Table table = super.getTable();
	    table.setLayoutData(new GridData(GridData.FILL_BOTH));
	    setContentProvider(new InternContentProvider());
	    setLabelProvider(new InternLabelProvider());
	    setComparator(new InternViewerSorter());
	    
	    TableColumn nameColumn = new TableColumn(table, SWT.LEFT);
	    nameColumn.setText(FIRST_COLUMN_HEAD_NAME);
	    nameColumn.setWidth(NAME_COLUMN_WIDTH);
	    nameColumn.addSelectionListener(new SelectionAdapter() {
	        public void widgetSelected(SelectionEvent event) {
	          ((InternViewerSorter) getComparator()).doSort(FIRST_COLUMN_NUMBER);
	          refresh();
	        }
	      });
	    
	    TableColumn groupColumn = new TableColumn(table, SWT.CENTER);
	    groupColumn.setText(SECOND_COLUMN_HEAD_NAME);
	    groupColumn.pack();
	    groupColumn.addSelectionListener(new SelectionAdapter() {
	        public void widgetSelected(SelectionEvent event) {
	          ((InternViewerSorter) getComparator()).doSort(SECOND_COLUMN_NUMBER);
	          refresh();
	        }
	      });
	    
	    TableColumn checkColumn = new TableColumn(table, SWT.CENTER | SWT.CHECK);
	    checkColumn.setText(THIRD_COLUMN_HEAD_NAME);
	    checkColumn.pack();
	    checkColumn.addSelectionListener(new SelectionAdapter() {
	        public void widgetSelected(SelectionEvent event) {
	          ((InternViewerSorter) getComparator()).doSort(THIRD_COLUMN_NUMBER);
	          refresh();
	        }
	      });
	    
	    table.setHeaderBackground(TABLE_HEAD_BACKGROUND_COLOR);
	    table.setHeaderVisible(true);
	    table.setLinesVisible(true);

	    JsonReader.readJson();
	    setInput(InternListService.getInternListService().getListInterns());
	    
	}
}
