package main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.providers;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Shell;

import main.java.com.luxoft.poltavec.JFaceHomeworkLog.model.Intern;

public class InternLabelProvider implements ITableLabelProvider {

	private final Image POSITIVEIMG = makeShot(true);
	private final Image NEGATIVEIMG = makeShot(false);
	
	
	// Draw checkBox
	private Image makeShot(boolean type) {
        Shell s = new Shell(SWT.NO_TRIM);
        Button b = new Button(s, SWT.CHECK);
        b.setSelection(type);
        Point bsize = b.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        b.setSize(bsize);
        b.setLocation(0, 0);
        s.setSize(bsize);
        s.open();

        GC gc = new GC(b);
        Image image = new Image(s.getDisplay(), bsize.x+2, bsize.y);
        gc.copyArea(image, -2, 0);
        gc.dispose();

        s.close();

        return image;
    }

	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		Intern Intern = (Intern) element;
		Image image = null;

		switch (columnIndex) {
		case 0:
			break;
		case 1:
			break;
		case 2:
			if (Intern.isSWTdone()) {
				image = POSITIVEIMG;
				break;
			} else {
				image = NEGATIVEIMG;
				break;
			}

		}
		return image;
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {
		Intern Intern = (Intern) element;
		String text = "";

		switch (columnIndex) {
		case 0:
			text = Intern.getName();
			break;
		case 1:
			text = String.valueOf(Intern.getGroup());
			break;
		case 2:
			break;
		}
		return text;
	}

	@Override
	public void addListener(ILabelProviderListener arg0) {
	}

	@Override
	public void dispose() {
	}

	@Override
	public boolean isLabelProperty(Object arg0, String arg1) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener arg0) {
	}

}
