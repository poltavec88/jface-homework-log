package main.java.com.luxoft.poltavec.JFaceHomeworkLog.view;

import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.AboutListener;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.ExitListener;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.SaveFileListener;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.EditListeners.SelectionAddListener;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.EditListeners.SelectionCancelListener;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.EditListeners.SelectionDeleteListener;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.EditListeners.SelectionSaveListener;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.Events;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.Notifiable;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.Notifier;

public class MyMenu implements Notifiable{

	private Menu menu;
	private ApplicationWindow parent;
	private MenuItem newItem;
	private MenuItem saveIt;
	private MenuItem deleteItem;
	private MenuItem cancelItem;
	private MenuItem aboutItem;
	private MenuItem exitItem;
	private MenuItem saveItem;
	
	public MyMenu(ApplicationWindow parent) {

		menu = new Menu(parent.getShell(), SWT.BAR);
		parent.getShell().setMenuBar(menu);
		this.parent = parent;

		initMenu();

		Notifier.getUIManagerService().addSubscriber(this);
	}

	@Override
	public void update(Events event) {
		switch(event) {
		case CANCELON:
			cancelItem.setEnabled(true);
			break;
		case CANCELOFF:
			cancelItem.setEnabled(false);
			break;
		case NEWON:
			newItem.setEnabled(true);
			break;
		case DELETEONSAVEOFF:
			deleteItem.setEnabled(true);
			saveIt.setEnabled(false);
			break;
		case SAVEONDELETEOFF:
			deleteItem.setEnabled(false);
			saveIt.setEnabled(true);
			break;
		case NEWSAVEDALETEOFF:
			newItem.setEnabled(false);
			deleteItem.setEnabled(false);
			saveIt.setEnabled(false);
			break;
		default:
			break;
		}
	}
	
	private void initMenu() {
		// File menu
		MenuItem file = new MenuItem(menu, SWT.CASCADE);
	    file.setText("&File");
	    
	    Menu filemenu = new Menu(parent.getShell(), SWT.DROP_DOWN);
	    file.setMenu(filemenu);
	    
	    saveItem = new MenuItem(filemenu, SWT.CASCADE);
	    saveItem.setText("&Save file");
	    saveItem.setAccelerator(SWT.CTRL + 'S');
	    
	    new MenuItem(filemenu, SWT.SEPARATOR);
	    
	    exitItem = new MenuItem(filemenu, SWT.PUSH);
	    exitItem.setText("E&xit");
	    exitItem.setAccelerator(SWT.CTRL + 'X');
	    
	    
	    // Edit menu
	    MenuItem edit = new MenuItem(menu, SWT.CASCADE);
	    edit.setText("&Edit");
	    
	    Menu editMenu = new Menu(parent.getShell(), SWT.DROP_DOWN);
	    edit.setMenu(editMenu);
	    
	    newItem = new MenuItem(editMenu, SWT.CASCADE);
	    newItem.setText("&Add new");
	    newItem.setEnabled(false);
	    
	    saveIt = new MenuItem(editMenu, SWT.CASCADE);
	    saveIt.setText("&Save");
	    saveIt.setEnabled(false);
	    
	    deleteItem = new MenuItem(editMenu, SWT.CASCADE);
	    deleteItem.setText("&Delete");
	    deleteItem.setEnabled(false);
	    
	    cancelItem = new MenuItem(editMenu, SWT.CASCADE);
	    cancelItem.setText("&Cancel");
	    cancelItem.setEnabled(false);
	    
	 // About menu
	    MenuItem help = new MenuItem(menu, SWT.CASCADE);
	    help.setText("&Help");
	    
	    Menu helpMenu = new Menu(parent.getShell(), SWT.DROP_DOWN);
	    help.setMenu(helpMenu);
	    
	    aboutItem = new MenuItem(helpMenu, SWT.CASCADE);
	    aboutItem.setText("&About");
	    aboutItem.setAccelerator(SWT.CTRL + 'I');
	}
	
	public void addListeners() {
		newItem.addSelectionListener(new SelectionAddListener());
		saveIt.addSelectionListener(new SelectionSaveListener());
		deleteItem.addSelectionListener(new SelectionDeleteListener());
		cancelItem.addSelectionListener(new SelectionCancelListener());
		saveItem.addSelectionListener(new SaveFileListener(parent));
		exitItem.addSelectionListener(new ExitListener(parent));
		aboutItem.addSelectionListener(new AboutListener(parent));
	}
}
