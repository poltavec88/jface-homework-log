package main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller;

import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.Events;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.Notifier;

/**
 * The DateModifyListener class implements ModifyListener and SelectionListener.
 * Here is the implementation of the logic for enabling and disabling the menu and editor UI buttons
 */	
public class DataModifyListener implements ModifyListener, SelectionListener {

	@Override
	public void modifyText(ModifyEvent e) {
		listen();
	}

	@Override
	public void widgetSelected(SelectionEvent e) {
		listen();
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		listen();
	}
	
	private void listen() {
		Notifier.getUIManagerService().notifySubscribers(Events.MODIFY);
	}
}
