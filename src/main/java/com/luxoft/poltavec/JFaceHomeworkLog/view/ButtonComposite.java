package main.java.com.luxoft.poltavec.JFaceHomeworkLog.view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.DataModifyListener;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.EditListeners.SelectionAddListener;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.EditListeners.SelectionCancelListener;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.EditListeners.SelectionDeleteListener;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.EditListeners.SelectionSaveListener;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.vertification.GroupVerfiticationListener;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.vertification.NameVertificationListener;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.model.Intern;
import main.java.com.luxoft.poltavec.JFaceHomeworkLog.service.ProvidableView;

public class ButtonComposite extends Composite implements ProvidableView {

	private final int BOX_SIZE = 1;
	private final int DOUBLE_BOX_SIZE = 2;
	private final int QUADRUPLE_BOX_SIZE = 4;
	private final int GRIDDATA_VERTICAL_INDENT = 50;
	private final int GRIDDATA_HORIZONTAL_INDENT = 30;
	private final int NUMBER_OF_COMPOSITE_COLUMNS = 4;
	private final String FIRST_BUTTON_NAME = "New";
	private final String SECOND_BUTTON_NAME = "Save";
	private final String THIRD_BUTTON_NAME = "Delete";
	private final String FOURTH_BUTTON_NAME = "Cancel";
	private final String FIRST_LABEL_NAME = DataTable.FIRST_COLUMN_HEAD_NAME + ":";
	private final String SECOND_LABEL_NAME = DataTable.SECOND_COLUMN_HEAD_NAME + ":";
	private final String THIRD_LABEL_NAME = DataTable.THIRD_COLUMN_HEAD_NAME;
	private final String GROUP_TOOLTIP_TEXT = "Number only allowed";
	private final String NAME_TOOLTIP_TEXT = "Letters only allowed";
	
	
	private Text textName;
	private Text textGroup;
	private Button SWTdoneButton;
	private Button newButton;
	private Button saveButton;
	private Button deleteButton;
	private Button cancelButton;

	public ButtonComposite(Composite parent) {
		super(parent, SWT.BORDER | SWT.MULTI | SWT.FULL_SELECTION);
		GridLayout gridLayOut = new GridLayout();
		gridLayOut.numColumns = NUMBER_OF_COMPOSITE_COLUMNS;
		gridLayOut.makeColumnsEqualWidth = true;
		setLayout(gridLayOut);
		createContents();
	}

	@Override
	public String getTextName() {
		return textName.getText();
	}

	@Override
	public String getTextGroup() {
		return textGroup.getText();
	}

	@Override
	public boolean getSWTdoneButton() {
		return SWTdoneButton.getSelection();
	}
	
	@Override
	public void setEnabledNewButton(boolean bool) {
		this.newButton.setEnabled(bool);
	}
	
	@Override
	public void setEnabledSaveButton(boolean bool) {
		this.saveButton.setEnabled(bool);
	}
	
	@Override
	public void setEnabledDeleteButton(boolean bool) {
		this.deleteButton.setEnabled(bool);
	}
	
	@Override
	public void setEnabledCancelButton(boolean bool) {
		this.cancelButton.setEnabled(bool);
	}

	@Override
	public void updateTexts(Intern intern) {
		SWTdoneButton.setSelection(intern.isSWTdone());
		textName.setText(intern.getName());
		textGroup.setText(intern.getGroup());
	}
	
	@Override
	public void addListeners() {
		newButton.addSelectionListener(new SelectionAddListener());
		saveButton.addSelectionListener(new SelectionSaveListener());
		deleteButton.addSelectionListener(new SelectionDeleteListener());
		cancelButton.addSelectionListener(new SelectionCancelListener());
		textName.addModifyListener(new DataModifyListener());
		textGroup.addModifyListener(new DataModifyListener());
		SWTdoneButton.addSelectionListener(new DataModifyListener());
	}

	private void createContents() {
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false, BOX_SIZE, BOX_SIZE);
		gridData.verticalIndent = GRIDDATA_VERTICAL_INDENT;
		gridData.horizontalIndent = GRIDDATA_HORIZONTAL_INDENT;

		GridData textGridData = new GridData(SWT.FILL, SWT.FILL, true, false, DOUBLE_BOX_SIZE, BOX_SIZE);
		textGridData.verticalIndent = GRIDDATA_VERTICAL_INDENT;
		textGridData.horizontalIndent = GRIDDATA_HORIZONTAL_INDENT;

		GridData SWTbuttonGridData = new GridData(SWT.FILL, SWT.FILL, true, false, QUADRUPLE_BOX_SIZE, BOX_SIZE);
		SWTbuttonGridData.verticalIndent = GRIDDATA_VERTICAL_INDENT;
		SWTbuttonGridData.horizontalIndent = GRIDDATA_HORIZONTAL_INDENT;

		GridData buttonGridData = new GridData(SWT.FILL, SWT.FILL, true, false, BOX_SIZE, BOX_SIZE);
		buttonGridData.verticalIndent = GRIDDATA_VERTICAL_INDENT;

		Label nameLabel = new Label(this, SWT.NULL);
		nameLabel.setText(FIRST_LABEL_NAME);
		nameLabel.setLayoutData(gridData);

		textName = new Text(this, SWT.BORDER);
		textName.setLayoutData(textGridData);
		textName.setToolTipText(NAME_TOOLTIP_TEXT);
		textName.addListener(SWT.Verify, new NameVertificationListener());
		
		
		Label delay = new Label(this, SWT.NULL);
		delay.setLayoutData(gridData);

		Label groupLabel = new Label(this, SWT.NULL);
		groupLabel.setText(SECOND_LABEL_NAME);
		groupLabel.setLayoutData(gridData);

		textGroup = new Text(this, SWT.BORDER);
		textGroup.setLayoutData(textGridData);
		textGroup.setToolTipText(GROUP_TOOLTIP_TEXT);
		textGroup.addListener(SWT.Verify, new GroupVerfiticationListener());

		SWTdoneButton = new Button(this, SWT.CHECK);
		SWTdoneButton.setText(THIRD_LABEL_NAME);
		SWTdoneButton.setLayoutData(SWTbuttonGridData);
		SWTdoneButton.setSelection(false);

		newButton = new Button(this, SWT.PUSH);
		newButton.setText(FIRST_BUTTON_NAME);
		newButton.setLayoutData(buttonGridData);
		newButton.setEnabled(false);

		saveButton = new Button(this, SWT.PUSH);
		saveButton.setText(SECOND_BUTTON_NAME);
		saveButton.setLayoutData(buttonGridData);
		saveButton.setEnabled(false);

		deleteButton = new Button(this, SWT.PUSH);
		deleteButton.setText(THIRD_BUTTON_NAME);
		deleteButton.setLayoutData(buttonGridData);
		deleteButton.setEnabled(false);

		cancelButton = new Button(this, SWT.PUSH);
		cancelButton.setText(FOURTH_BUTTON_NAME);
		cancelButton.setLayoutData(buttonGridData);
		cancelButton.setEnabled(false);
	}
}
