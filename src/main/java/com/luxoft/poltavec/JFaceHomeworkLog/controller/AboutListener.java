package main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller;

import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

/**
 * The AboutListener class implements SelectionListener.
 * Here is the implementation of the About button from the top menu
 */	
public class AboutListener implements SelectionListener {

	private ApplicationWindow parent;

	public AboutListener(ApplicationWindow applicationWindow) {
		this.parent = applicationWindow;
	}

	@Override
	public void widgetSelected(SelectionEvent e) {

		parent.setStatus("About window opened");

		Shell shell = parent.getShell();
		MessageBox dialog =new MessageBox(shell, SWT.ICON_INFORMATION | SWT.OK);
			dialog.setText("My info");
			dialog.setMessage("Author Poltavec");
		
		int result = dialog.open();
		
		switch (result) {
		case SWT.OK:
			parent.setStatus("Ready");
			break;
		case SWT.CLOSE:
			parent.setStatus("Ready");
			break;
		}
		
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		// TODO Auto-generated method stub

	}

}
