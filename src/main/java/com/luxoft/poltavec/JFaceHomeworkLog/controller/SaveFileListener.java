package main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller;

import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import main.java.com.luxoft.poltavec.JFaceHomeworkLog.DAO.JsonWriter;

/**
 * The SaveFileListener class implements SelectionListener.
 * Here is the implementation of the Save button from the top menu
 */	
public class SaveFileListener implements SelectionListener {
	
	private ApplicationWindow applicationWindow;

	public SaveFileListener(ApplicationWindow applicationWindow) {
		this.applicationWindow = applicationWindow;
	}
	
	@Override
	public void widgetSelected(SelectionEvent e) {

		applicationWindow.setStatus("Saved");
   	 	JsonWriter.writeJson();
   	 
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		// TODO Auto-generated method stub
		
	}

}
