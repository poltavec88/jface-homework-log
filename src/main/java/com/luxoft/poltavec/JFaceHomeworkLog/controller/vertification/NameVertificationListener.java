package main.java.com.luxoft.poltavec.JFaceHomeworkLog.controller.vertification;

import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

/**
 * The NameVertificationListener class implements Listener.
 * Here is the implementation of verifying the input Name in the editor UI
 */	
public class NameVertificationListener implements Listener {
	
	private final int LAST_LOWERCASE_CYRILLIC_LETTER_CHAR_NUMBER = 1103;
	private final int LAST_LOWERCASE_LATINIC_LETTER_CHAR_NUMBER = 122;
	private final int FIRST_LOWERCASE_CYRILLIC_LETTER_CHAR_NUMBER = 1072;
	private final int FIRST_LOWERCASE_LATINIC_LETTER_CHAR_NUMBER = 97;
	private final int LAST_UPPERCASE_CYRILLIC_LETTER_CHAR_NUMBER = 1071;
	private final int LAST_UPPERCASE_LATINIC_LETTER_CHAR_NUMBER = 90;
	private final int FIRST_UPPERCASE_CYRILLIC_LETTER_CHAR_NUMBER = 1040;
	private final int FIRST_UPPERCASE_LATINIC_LETTER_CHAR_NUMBER = 65;
	private final int SPACE_CHAR_NUMBER = 32;
	
	@Override
	public void handleEvent(Event e) {
		
		String string = e.text;
        char[] chars = new char[string.length()];
        string.getChars(0, chars.length, chars, 0);
        for (int i = 0; i < chars.length; i++) {
        	    	     
        	if (!(((FIRST_LOWERCASE_LATINIC_LETTER_CHAR_NUMBER <= chars[i] && chars[i] <= LAST_LOWERCASE_LATINIC_LETTER_CHAR_NUMBER) 
        		|| (FIRST_UPPERCASE_LATINIC_LETTER_CHAR_NUMBER <= chars[i] && chars[i] <= LAST_UPPERCASE_LATINIC_LETTER_CHAR_NUMBER) 
        		|| (chars[i] == SPACE_CHAR_NUMBER))
        		|| ((FIRST_LOWERCASE_CYRILLIC_LETTER_CHAR_NUMBER <= chars[i] && chars[i] <= LAST_LOWERCASE_CYRILLIC_LETTER_CHAR_NUMBER) 
        		|| (FIRST_UPPERCASE_CYRILLIC_LETTER_CHAR_NUMBER <= chars[i] && chars[i] <= LAST_UPPERCASE_CYRILLIC_LETTER_CHAR_NUMBER)))) {
        		
        		e.doit = false;
        		return;
        		
        	}
        }
	}

}
