package main.java.com.luxoft.poltavec.JFaceHomeworkLog.service;

import java.util.ArrayList;
import java.util.List;


public class Notifier {

	private List<Notifiable> subscribers = new ArrayList<>();
	private static Notifier uiManagerService;
	
	private Notifier() {}
	
	public static Notifier getUIManagerService() {
		if (uiManagerService == null) {
			return uiManagerService = new Notifier();
		}
		return uiManagerService;
	}
	
	public void addSubscriber(Notifiable uiManager) {
		this.subscribers.add(uiManager);
	}
	
	public void deleteSubscriber(Notifiable uiManager) {
		this.subscribers.remove(uiManager);
	}

	/**
	 * The add() method adds a data model to the list, updates the displayed list,
	 * and clears the input field in the editor
	 */
	public void notifySubscribers(Events event) {
		for (Notifiable uiManager : subscribers)
			uiManager.update(event);
	}
}
