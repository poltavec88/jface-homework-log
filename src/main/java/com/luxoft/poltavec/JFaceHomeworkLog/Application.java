package main.java.com.luxoft.poltavec.JFaceHomeworkLog;


import org.eclipse.swt.widgets.Display;

import main.java.com.luxoft.poltavec.JFaceHomeworkLog.view.Window;

/**
 * The Application class starts the UI.
 */	
public class Application {

	public static void main(String[] args) {
        Window win = new Window();
        win.setBlockOnOpen(true);
        win.open();             
        Display.getCurrent().dispose();
    } 
}